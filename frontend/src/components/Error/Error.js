import { useEffect } from 'react';
import { ToastContainer, toast } from 'react-toastify';
import { useDispatch, useSelector } from 'react-redux';
import 'react-toastify/dist/ReactToastify.css';
import { clearError, selectErrorMessage } from '../../redux/slices/errorSlice';

const Error = () => {
    const errrorMessage = useSelector(selectErrorMessage);
    const dispatch = useDispatch();

    useEffect(() => {
        if (errrorMessage) {
            toast.info(errrorMessage);
            dispatch(clearError());
        }
    }, [errrorMessage, dispatch]);
    return (
        <ToastContainer
            position="top-right"
            autoClose={2000}
        />
    );
};

export default Error;
