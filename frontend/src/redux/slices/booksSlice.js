import axios from 'axios';

import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import createBookWithID from '../../utils/createBookWithID';
import { setError } from './errorSlice';

const initialState = {
    books: [],
    isLoadingViaAPI: false,
};

export const fetchBoook = createAsyncThunk(
    'books/fetchBook',
    async (url, thunkAPI) => {
        try {
            const res = await axios.get(url);
            return res.data;
        } catch (error) {
            thunkAPI.dispatch(setError(error.message));
            return thunkAPI.rejectWithValue(error);
            // throw error;
        }
    }
);

const booksSlice = createSlice({
    name: 'books',
    initialState,
    reducers: {
        addBook: (state, action) => {
            state.books.push(action.payload);
        },
        deleteBook: (state, action) => {
            // const index = state.findIndex((book) => book.id === action.payload);
            // if (index !== -1) {
            //     state.splice(index, 1);
            // }
            return {
                ...state,
                books: state.books.filter((book) => book.id !== action.payload),
            };
        },
        toggleFavorite: (state, action) => {
            // return state.map((book) =>
            //     book.id === action.payload
            //         ? { ...book, isFavorite: !book.isFavorite }
            //         : book
            // );
            state.books.forEach((book) => {
                if (book.id === action.payload) {
                    book.isFavorite = !book.isFavorite;
                }
            });
        },
    },
    // option 1
    extraReducers: {
        [fetchBoook.pending]: (state, action) => {
            state.isLoadingViaAPI = true;
        },
        [fetchBoook.fulfilled]: (state, action) => {
            state.isLoadingViaAPI = false;
            if (action.payload.title && action.payload.author) {
                state.books.push(createBookWithID(action.payload, 'API'));
            }
        },
        [fetchBoook.rejected]: (state) => {
            state.isLoadingViaAPI = false;
        },
    },
    // option 2
    // extraReducers: (builder) => {
    //     builder.addCase(fetchBoook.pending, (state) => {
    //         state.isLoadingViaAPI = true;
    //     });
    //     builder.addCase(fetchBoook.fulfilled, (state, action) => {
    //         if (action.payload.title && action.payload.author) {
    //             state.books.push(createBookWithID(action.payload, 'API'));
    //         }
    //     });
    //     builder.addCase(fetchBoook.rejected, (state) => {
    //         state.isLoadingViaAPI = false;
    //     });
    // },
});

export const { addBook, deleteBook, toggleFavorite } = booksSlice.actions;

// export const thunkFunction = async (dispatch, getState) => {
//     // async actions
//     try {
//         const res = await axios.get('http://localhost:4000/random-book');
//         if (res?.data?.title && res?.data?.author) {
//             dispatch(addBook(createBookWithID(res.data, 'API')));
//         }
//     } catch (error) {
//         console.log('Error fetching random book', error);
//     }
// };

export const selectBooks = (state) => state.books.books;
export const selectIsLoadingViaAPI = (state) => state.books.isLoadingViaAPI;

export default booksSlice.reducer;
